import { Test, TestingModule } from '@nestjs/testing';
import { FileOutputController } from './file-output.controller';
import { FileOutputService } from './file-output.service';
import * as pactum from 'pactum';
import { AuthDto } from '../auth/dto';
import { PrismaService } from '../prisma/prisma.service';
import { ConfigService } from '@nestjs/config';

describe('FileOutputController', () => {
  let controller: FileOutputController;

  beforeAll(async () => {
    pactum.request.setBaseUrl('http://localhost:3333');
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FileOutputController],
      providers: [FileOutputService, PrismaService, ConfigService],
    }).compile();

    controller = module.get<FileOutputController>(FileOutputController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('file-output', () => {
    const testdto: AuthDto = {
      email: 'test@mail.com',
      password: '123'
    };

    it('signin', () => {
        return pactum.spec().post('/auth/signup').withBody(testdto)
        .stores('access_token', 'accessToken'); 
    });

    it('signup', () => {
      return pactum.spec().post('/auth/signin').withBody(testdto)
      .stores('access_token', 'accessToken');
    });

    describe('results', () => {
      it('doesnt`t accept invalid id', () => {
        return pactum.spec().get('/file-output/results?id=ag56&format=json').withHeaders({Authorization: 'Bearer $S{access_token}'}).expectStatus(400);
      });

      it('doesnt`t accept absent id', () => {
        return pactum.spec().get('/file-output/results?format=json').withHeaders({Authorization: 'Bearer $S{access_token}'}).expectStatus(400);
      });

      it('doesnt`t accept absent format', () => {
        return pactum.spec().get('/file-output/results?id=1').withHeaders({Authorization: 'Bearer $S{access_token}'}).expectStatus(400);
      });

      it('doesnt`t accept invalid format', () => {
        return pactum.spec().get('/file-output/results?id=1&format=png').withHeaders({Authorization: 'Bearer $S{access_token}'}).expectStatus(400);
      });
    });
  });
});
