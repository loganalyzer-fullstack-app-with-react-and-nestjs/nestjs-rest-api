import { Module } from '@nestjs/common';
import { FileOutputController } from './file-output.controller';
import { FileOutputService } from './file-output.service';

@Module({
  controllers: [FileOutputController],
  providers: [FileOutputService]
})
export class FileOutputModule {}
