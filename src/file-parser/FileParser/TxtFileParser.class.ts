import { BadRequestException } from "@nestjs/common";
import { ParsedFile, User } from "./FileParserFactory.class";



/**
 * It parses a log file and returns a map of users and their total time in seconds spent on the system
 *
 * @class TxtFileParser
 */
export class TxtFileParser {
    //Shared between files while parsing them
    private _parsedFiles: Map<string, number>;

    //Cleared for every new file while parsing it
    private _parsedUsers = new Map<string, User>();

    /**
     * It takes an array of objects, and returns an object with the same keys and values
     * 
     * @return An object with the same keys and values as the Map.
     */
    get parsedFile(): ParsedFile {
        return Object.fromEntries(this._parsedFiles.entries());
    }

    /**
     * @constructor
     * @throws If the grace period is less than 0, throw a BadRequestException
     * @param {number} gracePeriod - number - This is the grace period in seconds.
     */
    constructor(private gracePeriod: number) {
        if(this.gracePeriod < 0) throw new BadRequestException('Grace period should be a positive integer');
        this._parsedFiles = new Map<string, number>();
    }

    /**
     * @param {string | string[]} inputFileStream - string | string[]
     * @returns The parsedFile object.
     */
    public parse(inputFileStream: string | string[]): ParsedFile {
        if (Array.isArray(inputFileStream)) {
            for (let file of inputFileStream) {
                this.parseIndividualFile(file);
            }
        }
        else {
            this.parseIndividualFile(inputFileStream);
        }

        //Handles cases when there is no logout log in the file
        if (this._parsedUsers.size > 0) {
            for (let user of this._parsedUsers) {
                this.handleLogOut(user[0], user[1], this.gracePeriod);
            }
        }

        return this.parsedFile;
    }

    /**
     * Separates each word into separate element in a string array
     * @param {string} fileStr - string - The string that contains the file contents
     * @returns An array of strings.
     */
    private getArguments(fileStr: string): string[] {
        let lines = fileStr.split(/\r?\n/).filter(el => el); //Filter is to remove undefined, null, empty elements
        let args = new Array<string>();

        let timeStamp: string, userName: string, action: string[];
        for(let line of lines) {
            [timeStamp, userName, ...action] = line.split(' ').filter(el => el);
            let actionStr = action.join(' ');
            args.push(timeStamp, userName, actionStr);
        }
        return args;
    }

    /**
     * It takes a file, parses it, and then adds the parsed data to the map _parsedUsers.
     * @param {string} file - string - the file that is being parsed
     * @throws BadRequestException if it cannot parse the file or one of the timestamps is invalid
     */
    private parseIndividualFile(file: string) {
        let args = this.getArguments(file);

        for (let i = 0; i < args.length; i += 3) {
            let timeStamp = args[i];
            let userName = args[i + 1];
            let action = args[i + 2];

            if (timeStamp && userName && action) {
                if(!this.isValidDate(Number(timeStamp)))
                    throw new BadRequestException(`Date ${timeStamp} is not valid`);

                let act = action.toUpperCase();
                let user = this._parsedUsers.get(userName);

                if (act === 'LOGIN' && !user) {
                    this._parsedUsers.set(userName, {
                        lastTimeLoggedIn: Number(timeStamp),
                        secondsElapsed: 0
                    });
                }
                else if (act === 'LOGIN' && user) {
                    let updatedUser: User = {
                        secondsElapsed: user.secondsElapsed + this.gracePeriod,
                        lastTimeLoggedIn: Number(timeStamp),
                    }
                    this._parsedUsers.set(userName, updatedUser);
                }
                else {
                    this.calcSecondsElapsed(timeStamp, userName, act, user);
                }
            }
            else {
                throw new BadRequestException('Cannot parse file. Format should be <timestamp> <username> <action>');
            }
        }
    }

    /**
     * "If the user exists, calculate the seconds elapsed since the last time the user logged in,
     * update the user's lastTimeLoggedIn and secondsElapsed properties, and if the user logged out,
     * handle the logout."
     * @throws BadRequestException if timestamps are not linear in time
     * @param {string} timeStamp - string - the timestamp of the current log line
     * @param {string} userName - string - the user's name
     * @param {string} action - string - the action that the user is taking (LOGIN, LOGOUT, etc)
     * @param {User} user - User - the user object that was found in the parsedUsers map
     */
    private calcSecondsElapsed(timeStamp: string, userName: string, action: string, user: User) {
        if (user) {
            let secondsElapsed = Math.round((Number(timeStamp) - user.lastTimeLoggedIn));
            if(secondsElapsed < 0) 
                throw new BadRequestException(`
                    Log time must flow linearly top to bottom of log file. Timestamp ${timeStamp} should be > ${user.lastTimeLoggedIn} but it is not`);

            let updatedUser: User = {
                lastTimeLoggedIn: Number(timeStamp),
                secondsElapsed: user.secondsElapsed + secondsElapsed,
            };
            this._parsedUsers.set(userName, updatedUser);

            if (action === 'LOGOUT') {
                this.handleLogOut(userName, updatedUser); 
            }
        }
    }

    /**
     * it adds the user's
     * time spent to the parsed files map, and then it deletes the user from the parsed users map.
     * @param {string} userName - string - The name of the user
     * @param {User} user - User - This is the user object that is being logged out.
     * @param {number} [gracePeriod] - number - This is the amount of time in seconds that the user was
     * logged in for, but not actively using the computer.
     */
    private handleLogOut(userName: string, user: User, gracePeriod?: number) {
        let usersTimeSpent = this._parsedFiles.get(userName);
        let GracePeriodToAdd = ((gracePeriod) ? gracePeriod : 0);
        if (usersTimeSpent)
            this._parsedFiles.set(userName,
                usersTimeSpent + user.secondsElapsed + GracePeriodToAdd);
        else
            this._parsedFiles.set(userName,
                user.secondsElapsed + GracePeriodToAdd);

        this._parsedUsers.delete(userName);
    }

    /**
     * It checks if the timestamp is valid.
     * @param {number} timeStamp - number - The time stamp to be converted to a date.
     * @returns A boolean value.
     */
    private isValidDate(timeStamp: number): boolean {
        const miliseconds = timeStamp * 1000;
        const dateObj = new Date(miliseconds);
        return (dateObj.toString() !== 'Invalid Date');
    }
}