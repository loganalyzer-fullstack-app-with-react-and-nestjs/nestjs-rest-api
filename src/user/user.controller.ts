import { Controller, Get, HttpCode, HttpStatus, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { User } from '@prisma/client';
import { GetUser } from '../auth/decorator';
import { UserService } from './user.service';

@UseGuards(AuthGuard('jwt')) //Uses the JwtStrategy in auth/strategy to guard this route
@Controller('users')
export class UserController {
    constructor(private userService: UserService) {}

    @HttpCode(HttpStatus.OK)
    @Get('my-files')
    getMe(@GetUser() req: User) {
        return this.userService.getUserFiles(req);
    }
}
